# -*- coding: utf-8 -*-
from django.db import models
from uuid import uuid4


class GUIDModel(models.Model):
    guid = models.UUIDField(default=uuid4, editable=False, db_index=True, unique=True,
                            verbose_name='GUID объекта')
    created = models.DateTimeField(auto_now_add=True, null=True, verbose_name='Время создания объекта')
    updated = models.DateTimeField(auto_now=True, null=True, verbose_name='Время последнего обновления')

    class Meta:
        abstract = True
