# -*- coding: utf-8 -*-

from django.db import models

from utils.models import GUIDModel


class Region(GUIDModel):
    """
    Области
    """
    name = models.TextField(null=False, blank=True, default='', verbose_name='Наименование субъекта')
    symptom = models.TextField(null=False, blank=True, default='',)


class City(GUIDModel):
    """
    Города
    """
    city_name = models.TextField(null=False, blank=True, default='', verbose_name='Наименование города')
    area = models.TextField(null=False, blank=True, default='', verbose_name='Наименование района')
    region = models.ForeignKey(Region, on_delete=models.PROTECT, null=True, verbose_name='Наименование области')


class FederalDistrict(GUIDModel):
    """
    Федеральный округ
    """
    name = models.TextField(null=False, blank=True, default='', verbose_name='Федеральный округ')
    okato = models.IntegerField(null=True, verbose_name='ОКАТО')
    oktmo = models.IntegerField(null=True, verbose_name='ОКТМО')  # ToDo Уточнить RegExp для ОКТМО

    def __str__(self):
        return f'{self.name} (ОКТМО {self.oktmo})'
