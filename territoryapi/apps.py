from django.apps import AppConfig


class TerritoryapiConfig(AppConfig):
    name = 'territoryapi'
