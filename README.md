# Система ведения библиотек НИР

## Предназначение системы

Система предназначена для учёта библиотечных данных по НИР, в том числе для вывода статистики и поиска по НИР

## Разворачивание проекта

Разработка проекта ведётся на python 3.7, СУБД PostgreSQL v12. Ниже - небольшой гайд, чтобы развернуть проект для разработки.
* перейти в корневую папку проекта
* выполнить в консоли
```sh
python3 -m venv venv3
```
Должна создаться виртуальная среда под названием venv3.
* активировать виртуальную среду, выполнив в консоли    
-- для Linux
```sh
source venv3/bin/activate
```
-- для Windows
```sh
cd venv3/Scripts
activate
cd ../..
```
* установим зависимости, выполнив в консоли
```sh
pip install -r requirements.txt
```
Должно появиться сообщение об успешной установке пакетов, например
```sh
Successfully installed Django-3.0.8 asgiref-3.2.10 pytz-2020.1 sqlparse-0.3.1
```
* создадим базу данных:
```sh
> su postgres
Пароль:
postgres@> createdb sfour
postgres@> createuser your_user
postgres@> psql
postgres=\# alter user your_user with encrypted password 'your_password';
ALTER ROLE
postgres=\# grant all privileges on database sfour to your_user;
GRANT
postgres=\# \q
```
* Добавим файл с локальными настройками. Хранить параметры подключения в репозитории не принято. Когда кто-то коммитит свой пароль от базы в общий репозиторий, где-то плачет котёнок! Для добавления файла в папку проекта нужно вернуться к пользователю-владельцу папки.
```sh
su your_user
Пароль:
cd project && touch settings_local.py && cd ..
```
* Заполним этот файл локальными параметрами подключения по образцу:
```sh
nano  project/settings_local.py
```
```py
# settings_local.py
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'sfour',
        'USER': 'your_user',
        'PASSWORD': 'your_password',
        'HOST': '127.0.0.1',
        'PORT': '5432',
        }
    }
```

## Предназначение модулей

### territoryapi
Предназначен для заполнения и просмотра справочников по территориальному делению. [Подробнее...](territoryapi/README.md)

### accessapi
Предназначен для хранения профилей пользователей и структурных подразделений. [Подробнее...](accessapi/README.md)

### fileapi
Предназначен для учёта прикреплённых файлов. [Подробнее...](fileapi/README.md)
