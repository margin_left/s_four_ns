# -*- coding: utf-8 -*-

from django.contrib.auth.models import AbstractUser, Group
from django.contrib.postgres.fields import ArrayField, JSONField
from django.db import models

from utils.models import GUIDModel


class Department(GUIDModel):
    """
    Отдел
    """
    name = models.CharField(max_length=250, null=True, verbose_name='Наименование отдела')
    is_in_reports = models.BooleanField(default=False, null=False, verbose_name='Включать в отчёты')
    order_in_report = models.IntegerField(null=False, default=0, verbose_name='Порядковый номер в отчёте')
    is_deleted = models.BooleanField(default=False, null=False, verbose_name='Не актуален')
    number = models.IntegerField(null=False, default=0, verbose_name='Номер отдела')

    def __str__(self):
        return self.name


class Party(models.Model):
    """
    Рабочая группа
    """
    name = models.CharField(max_length=250, null=False, blank=False, verbose_name='Наименование рабочей группы')
    department = models.ForeignKey(Department, on_delete=models.PROTECT, null=True, verbose_name='Отдел')

    def __str__(self):
        return f'{self.name} ({self.department})'


class EngineeringWork(models.Model):
    """
    Технические работы
    """
    is_active = models.BooleanField(default=False, null=False, verbose_name='Проводятся технические работы')


class Management(models.Model):
    """
    Управление
    """
    name = models.CharField(max_length=250, null=False, blank=False, verbose_name='Наименование управления')

    def __str__(self):
        return self.name


class NTC(models.Model):
    """
    НТЦ
    """
    name = models.CharField(max_length=250, null=False, blank=False, verbose_name='Наименование НТЦ')

    def __str__(self):
        return self.name


class Position(models.Model):
    """
    Должность
    """
    name = models.TextField(null=False, blank=False, verbose_name='Наименование должности')
    is_deleted = models.BooleanField(null=False, default=False, verbose_name='Не актуальна')

    def __str__(self):
        return self.name


class UsersProfile(AbstractUser):
    """
    Пользователи
    """
    patronymic = models.CharField(max_length=100, null=False, blank=True, default='', verbose_name='Отчество')
    is_online = models.BooleanField(default=False, null=False, verbose_name='Сейчас в системе')
    last_online = models.DateTimeField(null=True, verbose_name='Время последнего логина')
    location = models.CharField(max_length=150, null=False, blank=True, default='', verbose_name='Местонахождение')
    phone_array = ArrayField(
        models.CharField(null=False, max_length=20, default=''),
        null=False, default=list, verbose_name='Номера телефонов')
    note_array = ArrayField(
        models.CharField(null=False, max_length=100, default=''),
        null=False, default=list, verbose_name='Пометки')
    engineering_work = models.ForeignKey(
        EngineeringWork, on_delete=models.SET_NULL, null=True, related_name='users', verbose_name='Технические работы')
    groups = models.ManyToManyField(Group, blank=True, verbose_name='Группы пользователя')

    # Справочники
    department = models.ForeignKey(Department, on_delete=models.PROTECT, null=True, verbose_name='Отдел')
    is_department_leader = models.BooleanField(null=False, default=False, verbose_name='Руководитель')
    position = models.ForeignKey(Position, on_delete=models.PROTECT, null=True, verbose_name='Должность')
    ntc = models.ForeignKey(NTC, on_delete=models.SET_NULL, null=True, verbose_name='НТЦ')
    management = models.ForeignKey(Management, on_delete=models.SET_NULL, null=True, verbose_name='Управление')
    party = models.ForeignKey(Party, on_delete=models.SET_NULL, null=True, verbose_name='Рабочая группа')
    is_party_leader = models.BooleanField(null=False, default=False, verbose_name='Руководитель рабочей группы')
    mac_level = models.IntegerField(null=False, default=0, verbose_name='Доступ')

    avatar = models.FileField(max_length=1024, upload_to='user_avatars/', null=True, verbose_name='Аватар профиля')
    theme = JSONField(null=True, verbose_name='Тема сайта')
    created = models.DateTimeField(auto_now_add=True, null=True, verbose_name='Дата создания аккаунта')

    def __str__(self):
        return self.full_name()

    def full_name(self):
        return ' '.join(filter(None, [self.last_name, self.first_name, self.patronymic]))

    def last_name_with_initials(self):
        result = self.last_name.title() + ' ' if self.last_name else ''
        result += ' '.join(map(lambda x: x[0].capitaize() + '.', filter(None, [self.first_name, self.patronymic])))
        return result
